
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<ctype.h>

#define CMDSIZE 259
#define BUFFERSIZE 15003
#define WORKPWD "/net/gcs/105/0556546/network_programming/np_project1_0556546/ras/"
#define PROMPT "% "
#define SOCKET_FD 1
#define STDERR 2

int pipe_number_entry[1000][2] = {0};

int find_empty_entry();
int read_to_new_line();
int readline(int fd, char* cmd, int maxlen);
char** cmdtok(char *cmds_in_line, const char delim, int *cmds_count);
void write_to_file(char **args, int arg_count, int *to_file);
int pipe_to_next_line(char *last_args, int *pipe_number);
int exec_shell_command(char *args[], int is_piped_cmd, int cmds_count, int *pipe_read_fd, int to_file, int flag);
void free_2Darray(char **array, int count);

int main(int argc, char** argv) {

    const char Welcome_message[3][42] =
    {
        {"****************************************\r\n"},
        {"** Welcome to the information server. **\r\n"},
        {"****************************************\r\n"}
    };
    char cmds[BUFFERSIZE] = {'\0'};
    char **cmd_split= NULL;
    int cmdline_len = 0;
    int cmds_count = 0;
    int i = 0;
    int stop = 0;
    int to_file = 0;
    int pipe_read_fd = -1;
    int arg_count = 0;
    char **arg_split = 0;
    int flag = 0;
    int pipe_number = 0;
    int status = 0;
    int index = 0;

    chroot(WORKPWD);
    chdir(WORKPWD); 
    putenv("PATH=bin:.");

    write(SOCKET_FD, Welcome_message, strlen(*Welcome_message));

    while (stop != 1)
    {
        write(SOCKET_FD, PROMPT, strlen(PROMPT));
        memset(cmds, '\0', BUFFERSIZE);
        cmdline_len = readline(SOCKET_FD, cmds, BUFFERSIZE);
        if (cmdline_len == 0) continue;
        for (i = 0; i < cmdline_len; i++) {
            if (cmds[i] == '|') {
                if (((i + 1) < cmdline_len) && isdigit(cmds[i + 1])) {
                    cmds[i] = '?';
                }
            }
        }
        cmd_split = cmdtok(cmds, '|', &cmds_count);
        pipe_read_fd = -1;
        pipe_read_fd = read_to_new_line();
        for (i = 0; i < cmds_count; i++) {
            arg_split = cmdtok(cmd_split[i], ' ', &arg_count);
            to_file = 0;
            write_to_file(arg_split, arg_count, &to_file);
            flag = pipe_to_next_line(arg_split[arg_count - 1], &pipe_number);
            if (flag != 0) arg_split[arg_count - 1] = 0;
            if (strncmp(arg_split[0], "exit", 4) == 0) {
                stop = 1;
            } else if (strncmp(arg_split[0], "setenv", 6) == 0) {
                setenv(arg_split[1], arg_split[2], 1);
            } else if (strncmp(arg_split[0], "printenv", 8) == 0) {
                char msg[BUFFERSIZE] = {'\0'};
                snprintf(msg, BUFFERSIZE, "PATH=%s\r\n", getenv(arg_split[1]));
                write(SOCKET_FD, msg, strlen(msg));
            } else {
                status = exec_shell_command(arg_split, i, cmds_count, &pipe_read_fd, to_file, flag);
                if (status != 0) break;
                if ((i == (cmds_count - 1)) && (flag != 0)) {
                    index = find_empty_entry();
                    if (index < 0) {
                        perror("pipe_number_table is full.\r\n");
                        break;
                    }
                    pipe_number_entry[index][0] = pipe_read_fd;
                    pipe_number_entry[index][1] = pipe_number;
                    index = 0;
                    flag = 0;
                }
            }
            free_2Darray(arg_split, arg_count);
        }
        free_2Darray(cmd_split, cmds_count);
    }
    close(SOCKET_FD);
    exit(0);
}

int find_empty_entry() {
    int i;
    for (i = 0; i < 1000; i++) {
        if (pipe_number_entry[i][1] == 0)
            return i;
    }
    return -1;
}

int read_to_new_line() {
    int pipefd[2];
    int i = 0;
    int flag = 0;
    char buffer[BUFFERSIZE] = {'\0'};
    if ((pipe(pipefd)) == -1) {
        perror("ERROR on pipe.\n");
    }
    for (i = 0; i < 1000; i++) {
        if (pipe_number_entry[i][1] > 0) {
            if (pipe_number_entry[i][1] == 1) {
                flag = 1;
                read(pipe_number_entry[i][0], buffer, BUFFERSIZE);
                close(pipe_number_entry[i][0]);
                pipe_number_entry[i][0] = 0;
                write(pipefd[1], buffer, strlen(buffer));
            }
            pipe_number_entry[i][1]--;
        }
        memset(buffer, '\0', BUFFERSIZE);
    }
    close(pipefd[1]);
    if (flag == 1) {
        return pipefd[0];
    } else {
        close(pipefd[0]);
        return -1;
    }
}

int readline(int fd, char* cmd, int maxlen)
{
    int checked = 0, i = 0;
    char c = '\0';
    int wc = 0;

    for (i = 1; i < maxlen; i++) {
        if ((checked = read(fd, &c, 1)) == 1) {
            if (c == '\n') {
                i--;
                break;
            } else if (c == '\r') {
                i--;
                continue;
            }
            *cmd++ = c;
            wc++;
        } else if (checked == 0) {
            break;
        } else return -1;
    }
    *cmd = 0;
    return (wc);
}

char** cmdtok(char *cmds_in_line, const char delim, int *cmds_count)
{
    char **result = 0;
    char *cptr = cmds_in_line;
    char *token = 0;
    char *rest_of_cmd = NULL;
    int index = 0;
    int flag = 0;

    *cmds_count = 0;

    while (*cptr) {
        if ((*cptr != delim) && (flag == 0)) {
            (*cmds_count)++;
            flag = 1;
        } else if ((*cptr == delim) && (flag == 1)) {
            flag = 0;
        }
        cptr++;
    }
    while (result == NULL) {
        result = malloc(sizeof(char*) * ((*cmds_count) + 1));
    }
    token = strtok_r(cmds_in_line, &delim, &rest_of_cmd);

    for (index = 0; index < *cmds_count; index ++) {
        result[index] = strdup(token);
        token = strtok_r(rest_of_cmd, &delim, &rest_of_cmd); 
    }
    result[index] = NULL;

    return result;
}

void write_to_file(char **args, int arg_count, int *to_file)
{
    int i = 0;
    for (i = 0; i < arg_count; i++) {
        if (strncmp(args[i], ">", 1) == 0) {
            fopen(args[i + 1], "w");
            *to_file = i;
            break;
        }
    }
}

void free_2Darray(char **array, int count)
{
    int i = 0;
    for (i = 0; i < count; i++) {
        if (array[i]) free(array[i]);
    }
    free(array);
}

int pipe_to_next_line(char *last_arg, int *pipe_number)
{
    char *tmp = (last_arg + 1);
    if (last_arg[0] == '?') {
        *pipe_number = atoi(tmp);
        return 1;
    } else if (last_arg[0] == '!') {
        *pipe_number = atoi(tmp);
        return 2;
    } else {
        return 0;
    }
}

int exec_shell_command(char *args[], int is_piped_cmd, int cmds_count, int *pipe_read_fd, int to_file, int flag)
{
    pid_t childpid = 0;
    char err_msg[CMDSIZE] = {'\0'};
    FILE *fp;
    int status = 0;
    int pipefd[2];

    if (pipe(pipefd) == -1) {
        perror("ERROR on pipe.\r\n");
    }
    if ((childpid = fork()) < 0) {
        perror("ERROR on fork.\r\n");
        exit(-1);
    } else if (childpid == 0) {
        close(pipefd[0]);
        dup2(*pipe_read_fd, 0);
        if (to_file) {
            fp = fopen(args[to_file + 1], "w");
            args[to_file] = NULL;
            args[to_file + 1] = NULL;
            dup2(fileno(fp), SOCKET_FD);
        } else if (is_piped_cmd < (cmds_count - 1)) {
            dup2(pipefd[1], SOCKET_FD);
        } else if (flag == 1) {
            dup2(pipefd[1], SOCKET_FD);
        } else if (flag == 2) {
            dup2(pipefd[1], SOCKET_FD);
            dup2(pipefd[1], STDERR);
        } 
        execvp(args[0], &args[0]);
        exit(-1);
    } else {
        wait(&status);
        if (status != 0) {
            snprintf(err_msg, CMDSIZE, "Unknown command: [%s].\r\n", args[0]);
            write(SOCKET_FD, err_msg, strlen(err_msg));
        }
        if (*pipe_read_fd >= 0) {
            close(*pipe_read_fd);
        }
        *pipe_read_fd = 0;
        *pipe_read_fd = pipefd[0];
        close(pipefd[1]);
    }
    return status;
}
