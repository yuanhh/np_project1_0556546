
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>

#define BUFSIZE 1501
#define SERVICE_PORT 7003

void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int listen_fd = 0, conn_fd = 0, err = 0;
    int optval = 1;
    int childpid = 0;

    struct sockaddr_in server, client;

    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_fd < 0) error("ERROR on opening socket!\n");

    bzero(&server, sizeof(server));
    bzero(&client, sizeof(client));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htons(INADDR_ANY);
    server.sin_port = htons(SERVICE_PORT);

    setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    err = bind(listen_fd, (struct sockaddr *) &server, sizeof(server));
    if (err < 0) error("ERROR on binding!\n");

    err = listen(listen_fd, 10);
    if (err < 0) error("ERROR on listen!\n");

    while (1)
    {
        socklen_t client_len = sizeof(client);
        conn_fd = accept(listen_fd, (struct sockaddr*) &client, &client_len);
        if (conn_fd < 0) error("ERROR on establishing new connection!\n");

        if ((childpid = fork()) < 0) {
            error("ERROR on fork.\n");
        } else if (childpid == 0) {
            close(listen_fd);
            dup2(conn_fd, 1);
            dup2(conn_fd, 2);
            execl("/net/gcs/105/0556546/network_programming/np_project1_0556546/ras_service", "1", (char *)NULL);
        } else {
            close(conn_fd);
        }
    }

    close(listen_fd);
    wait(NULL);
    return 0;
}

